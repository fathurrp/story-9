from django.contrib import admin
from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
	path('', views.landingpage, name='landingpage'),
    path('login/', views.loginpage, name='loginpage'),
    path('menu/', views.menupage, name='menupage'),
    path('logout/',views.logoutpage, name='logoutpage'),
]